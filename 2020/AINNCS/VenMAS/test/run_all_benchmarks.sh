#!/bin/bash

for f in *.py;
do
  if [[ $f == run* ]];
  then
    echo "Running test script $f"
    python3 $f >> "{$f}_out.log"
  fi
done

