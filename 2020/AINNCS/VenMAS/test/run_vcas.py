#!/usr/bin/env python

import sys
sys.path.append('../')

import datetime
import os

from resources.vcas.vcasagent import VcasAgent
from resources.vcas.vcasenv import VcasEnv, VcasConstants
from src.network_parser.network_model import NetworkModel
from src.utils.formula import *
from src.verification.bounds.bounds import HyperRectangleBounds

from common import mono_verify


def initialise_and_get_agent_and_env():
    """
    Initialise agent and environment.
    :return: Initialised VcasAgent and VcasEnv objects.
    """

    # Where the agent networks are.
    REL_PATH = "../resources/vcas/models/vcas_{}.h5"

    network_models = []
    for i in range(VcasConstants.N_ADVISORIES):
        nmodel = NetworkModel()
        script_dir = os.path.dirname(__file__)
        nmodel.parse(os.path.join(script_dir, REL_PATH).format(i + 1))
        network_models.append(nmodel)
    agent = VcasAgent(network_models)
    env = VcasEnv()

    return agent, env


def main():
    # Constraint specific variables of the initial state to one value by setting the upper
    # bounds equal to the lower bounds.
    initial_advisory = VcasConstants.COC
    initial_acceleration = VcasConstants.G / 10
    initial_tau = 25
    initial_altitude = -131
    noise = 2

    agent, env = initialise_and_get_agent_and_env()

    for initial_climbrate in [-19.5, -22.5, -25.5, -28.5]:
        for steps in range(1,11):
            print("###########", steps, "time steps;", initial_climbrate, "initial climbrate ##########")
            safe = AtomicDisjFormula(VarConstConstraint(StateCoordinate(VcasConstants.INTRUDER_ALTITUDE), GT, 100),
                                     VarConstConstraint(StateCoordinate(VcasConstants.INTRUDER_ALTITUDE), LT, -100))
            safety_formula = ANextFormula(steps, safe)


            unzipped = zip(*[(initial_altitude - noise, initial_altitude + noise),
                             (initial_climbrate,        initial_climbrate),
                             (initial_tau,              initial_tau),
                             (initial_acceleration,     initial_acceleration),
                             (initial_advisory,         initial_advisory)])

            input_hyper_rectangle = HyperRectangleBounds(*unzipped)

            log_info = mono_verify(safety_formula, input_hyper_rectangle, agent, env, timeout=3600)
            with open("arch-comp.log", "a") as file:
                file.write(
                    f"{datetime.datetime.now()}, VCAS_{initial_climbrate}, {steps}, {log_info[0]:9.6f}, {log_info[1]}\n")

            print("")


if __name__ == "__main__":
    main()
