function R = guardIntersect_pancake(obj,R0,guard,guardID,options)
% guardIntersect_pancake - implementation of the time scaling approach
%                          described in [1]
%
% Syntax:  
%    R = guardIntersect_pancake(obj,R0,guard,options)
%
% Inputs:
%    obj - object of class location
%    R - list of intersections between the reachable set and the guard
%    guard - guard set (class: constrained hyperplane)
%    guardID - ID of the guard set
%    options - struct containing the algorithm settings
%
% Outputs:
%    R - set enclosing the guard intersection
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   [1] S. Bak et al. "Time-Triggered Conversion of Guards for Reachability
%       Analysis of Hybrid Automata"

% Author:       Niklas Kochdumper
% Written:      05-November-2018             
% Last update:  20-November-2019
% Last revision:---

%------------- BEGIN CODE --------------

    sys = obj.contDynamics;

    % check if guard set is a constrained hyperplane
    if ~isa(guard,'conHyperplane')
       error('The method ''pancake'' only supports guards given as conHyperplane objects!'); 
    end
    
    % convert hyperplane to a halfspace that represents the outside of the
    % invariant set
    c = center(R0);
    hs = halfspace(guard.h.c,guard.h.d);

    if in(hs,c)
        hs = halfspace(-guard.h.c,-guard.h.d);
    end

    % set default options for nonlinear system reachability analysis
    if ~isa(sys,'nonlinearSys')
       options = defaultOptions(options,sys.dim); 
    end
    
    % create system for the time-scaled system dynamics
    [sys_,options] = scaledSystem(sys,hs,R0,guardID,options);

    % compute the reachable set for the time scaled system 
    R = reachTimeScaled(sys_,hs,R0,options);
    
    % jump accross the guard set in only one time ste
    R = jump(sys,hs,R,options);

    % project the reachable set onto the hyperplane
    R = projectOnHyperplane(guard,R);

end


% Auxiliary Functions -----------------------------------------------------

function [sys,options] = scaledSystem(sys,hs,R0,guardID,options)
% Scale the system dynamics using the distance to the hyperplane as a 
% scaling factor 

    % get maximum distance of initial set ot hyperplane
    maxDist = supremum(interval(hs.c' * R0 + (-hs.d)));
    options.paramInt = maxDist;

    % define scaling function
    g = @(x,p) (hs.c' * x - hs.d)./p;

    % get system dynamics
    n = sys.dim;
    m = sys.nrOfInputs;
    
    if isa(sys,'linearSys')
       f = @(x,u) dynamicsLinSys(x,u,sys); 
    else
       f = sys.mFile; 
    end

    % time scaled system dynamics
    F = @(x,u,p) g(x,p) * f(x,u);

    % create symbolic variables
    xSym = sym('x',[n,1]);
    uSym = sym('u',[m,1]);
    pSym = sym('p',1);

    % create file path
    name = ['generated_',sys.name,'_',num2str(guardID),'_timeScaled'];
    path = [coraroot filesep 'models' filesep 'Cora' filesep name];

    % create file for time scaled dynamics
    func = F(xSym,uSym,pSym);
    matlabFunction(func,'File',path,'Vars',{xSym,uSym,pSym});

    % create time scaled system
    str = ['sys = nonlinParamSys(n,m,1,[@' name '],options);'];
    eval(str);
end

function R = reachTimeScaled(sys,hs,R0,options)
% Compute the reachable set of the scaled system such that the final
% reachable set until the scaled reachable set is very close to the
% hyperplane

    % define threshold distance when scaling is stopped
    d_ = (supportFunc(R0,hs.c,'lower')-hs.d)/100;

    % initialize variables
    R = R0;
    R_ = R0;
    
    d = inf;
    
    % scale until hyperplane is intersected or minimum distance is reached
    while d > 0

       R = R_;

       % compute reachable set
       Rnext = initReach(sys,R,options);
       
       if length(Rnext.tp) > 1
           error('Splitted sets are not supported for this algorithm!');
       else
           R_ = Rnext.tp{1}.set;
       end

       % check if minimum distance is reached
       d = supportFunc(R_,hs.c,'lower') - hs.d;
       if d < d_
          break; 
       end
    end
end

function Rcont = jump(sys,hs,R0,options)
% compute the reachable set in such a way that the reachable set jumps in 
% only one time step accross the hyperplane    

    % initialize search domain for optimal time step size
    lb = 0;
    dist_ = inf;

    % increase time step until the hyperplane has been crossed
    while true
       
        % compute reachable set
        Rnext = initReach(sys,R0,options);
       
        if iscell(Rnext.tp)
            if length(Rnext.tp) > 1
                error('Splitted sets are not supported for this algorithm!');
            else
                Rcont = Rnext.ti{1};
                R = Rnext.tp{1}.set;
            end
        else
            Rcont = Rnext.ti;
            R = Rnext.tp;
        end
        
        % compute distance from hyperplane
        dist = supportFunc(R,hs.c,'upper') - hs.d;
        
        if dist < 0
            ub = options.timeStep;
            break;
        end
        
        % check for convergence
        if dist > dist_
           error('Pancake approach failed!'); 
        else
           dist_ = dist;
        end
        
        % update time step
        lb = options.timeStep;
        options.timeStep = 2 * options.timeStep;
        
        for i=1:(options.taylorTerms+1)
            options.factor(i)= options.timeStep^(i)/factorial(i);    
        end
    end

    % iteratively decrease time step size to find optimal time step
    N = 10;
    
    for j = 1:N 

        % update time step
        options.timeStep = (ub + lb)/2;

        for i=1:(options.taylorTerms+1)
            options.factor(i)= options.timeStep^(i)/factorial(i);    
        end

        % compute reachable set
        Rnext = initReach(sys,R0,options);
       
        if iscell(Rnext.tp)
            if length(Rnext.tp) > 1
                error('Splitted sets are not supported for this algorithm!');
            else
                Rcont_ = Rnext.ti{1};
                R = Rnext.tp{1}.set;
            end
        else
            Rcont_ = Rnext.ti;
            R = Rnext.tp;
        end

        % compute distance from hyperplane
        dist = supportFunc(R,hs.c,'upper') - hs.d;

        % check for convergence
        if dist < 0
           Rcont = Rcont_;
           ub = options.timeStep;
        else
           lb = options.timeStep;
        end
    end
end

function options = defaultOptions(options,dim)
% set default options for nonlinear system reachability analysis (required
% if the continuous dynamics of the hybrid automaton is linear)

    % algorithm
    if ~isfield(options,'alg')
       options.alg = 'lin'; 
    end

    % tensor order
    if ~isfield(options,'tensorOrder')
        options.tensorOrder = 3;
    end
    
    % error order
    if ~isfield(options,'errorOrder')
        options.errorOrder = 5;
    end
    
    % intermediate order
    if ~isfield(options,'intermediateOrder')
       options.intermediateOrder = options.zonotopeOrder; 
    end
    
    % max error
    if ~isfield(options,'maxError')
       options.maxError = 1e20 * ones(dim,1); 
    end

    % reduction interval
    if ~isfield(options,'reductionInterval')
       options.reductionInterval = inf; 
    end
end

function f = dynamicsLinSys(x,u,sys)
% dynamic function of a linear system

    if isempty(sys.c)
       f = sys.A * x + sys.B * u; 
    else
       f = sys.A * x + sys.B * u + sys.c;
    end
end
    
%------------- END OF CODE --------------