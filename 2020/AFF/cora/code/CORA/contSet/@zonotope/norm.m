function [val,x] = norm(Z)
% norm - computes the maximum zonotope norm
%NOTE: replaces the old norm function which gave wrong results
%
% Syntax:  
%    [val] = norm(Z)
%
% Inputs:
%    Z - zonotope object
%
% Outputs:
%   val - norm value of vertex with biggest distance from the center
%   x   - vertex attaining maximum norm
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: minnorm

% Author:       Victor Ga�mann
% Written:      18-September-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
if ~isYalmipInstalled()
    error('YALMIP must be on the MATLAB search path to use this function');
end
%%%ATTENTION: (1) is a Mixed Integer Quadratic Program, thus not very
%%%scalable!
%(1) norm(Z)^2 = max_{|u|<=1} u'*G'*G*u 
G = generators(Z);
[n,m] = size(G);
GG = G'*G;
%equivalent transformation of (1) to -min_{u\in{-1,1}^m} (u'*M*u)+lmax*m
lmax = max(eig(GG));
M =  GG - lmax*eye(m);%+lmax*m
Mt = -M;
b = binvar(m,1);
obj = 4*(b-0.5)'*Mt*(b-0.5);
options = sdpsettings('verbose',0);
%use gurobi solver for MUCH faster solve times
%options.solver = 'gurobi';
%solve optimization problem
optimize([], obj, options);
val = sqrt(value(-obj + m*lmax));
x = G*value(2*(b-0.5))+center(Z);
%------------- END OF CODE --------------