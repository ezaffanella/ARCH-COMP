function pgon = polygon(pZ,varargin)
% polygon - creates a polygon enclosure of a 2-dimensional polyZonotope
%
% Syntax:  
%    pgon = polygon(pZ)
%    pgon = polygon(pZ,splits)
%
% Inputs:
%    pZ - polyZonotope object
%    splits - number of splits for refinement (optional)
%
% Outputs:
%    pgon - MATLAB polyshape object
%
% Example: 
%    pZ = polyZonotope([0;0],[2 0 1;0 2 1],[0;0],[1 0 3;0 1 1]);
%    pgon = polygon(pZ,8);
%
%    plot(pgon);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plotFilled, plot

% Author:       Niklas Kochdumper
% Written:      08-April-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % parse input arguments
    splits = 8;

    if nargin > 1 && ~isempty(varargin{1})
       splits = varargin{1}; 
    end
    
    % check if polynomial zonotope is two dimensional
    if dim(pZ) ~= 2
       error('Method "polygon" is only applicable for 2D polyZonotopes!');
    end

    % check if polynomial zonotope is a convex hull
    [res,pZ1,pZ2] = isConvHull(pZ);
   
    if res == 1
        try
            pgon = polygonConvHull(pZ1,pZ2,splits);
        catch
            pgon = polygonSingle(pZ,splits); 
        end
    else
        pgon = polygonSingle(pZ,splits); 
    end
end


% Auxiliary Functions -----------------------------------------------------

function [pgon,list] = polygonSingle(pZ,splits)
% converts a single polynomial zonotope to a polygon object

    % split the polynomial zonotope multiple times to obtain a better 
    % over-approximation of the real shape
    pZsplit{1} = pZ;
    pZfin = {};

    for i=1:splits
        pZnew = [];
        for j=1:length(pZsplit)
            try
                res = splitLongestGen(pZsplit{j});
                pZnew{end+1} = res{1};
                pZnew{end+1} = res{2};
            catch
                pZfin{end+1} = pZsplit{j};
            end
            
        end
        pZsplit = pZnew;
    end
    
    pZsplit = [pZsplit,pZfin];

    % over-approximate all splitted sets with zonotopes, convert them to 2D
    % polytopes (Matlab build-in) and compute the union
    warOrig = warning;
    warning('off','all');
    
    list = cell(length(pZsplit),1);

    for i = 1:length(pZsplit)

        % zonotope over-approximation
        zono = zonotope(pZsplit{i});

        % calculate vertices of zonotope
        vert = vertices(zono);

        % tranform to 2D polytope
        pgon = polyshape(vert(1,:),vert(2,:));
        list{i} = pgon;

        % calculate union with previous sets
        if i == 1
           polyAll = pgon; 
        else
           polyAll = union(polyAll,pgon); 
        end  
    end

    pgon = polyAll;
    
    warning(warOrig);
end

function pgon = polygonConvHull(pZ1,pZ2,splits)    
% compute a polygon for the convex hull of two polynomial zonotopes

    % check if first polygon is a convex hull
    [res1,pZ1_1,pZ1_2] = isConvHull(pZ1);
    
    if res1 == 1
        pgon1 = polygonConvHull(pZ1_1,pZ1_2,splits); 
        list1 = pgon2list(pgon1);
    else
        [~,list1] = polygonSingle(pZ1,splits);
    end

    % check if second polygon is a convex hull
    [res2,pZ2_1,pZ2_2] = isConvHull(pZ2);
    
    if res2 == 1
        pgon2 = polygonConvHull(pZ2_1,pZ2_2,splits); 
        list2 = pgon2list(pgon2);
    else
        [~,list2] = polygonSingle(pZ2,splits);
    end

    % compute convex hull of the two polygons
    pgon = convHull2D(list1,list2);
    
end

function list = pgon2list(pgon)
% convert a polgon to a list of triangles

    % compute triangluation
    T = triangulation(pgon);
    
    points = T.Points;
    con = T.ConnectivityList;
    
    % convert all triangles to polgon objects
    list = cell(size(con,2),1);
    
    for i = 1:size(con,2)
        list{i} = polyshape(points(con(i,:),:));
    end
end

function [res,pZ1,pZ2] = isConvHull(pZ)
% checks if polynomial zonotope is a convex hull of two polynomial
% zonotopes. If it is a convex hull, the function returns the two
% polynomial zonotopes

    res = 0;
    pZ1 = [];
    pZ2 = [];
    
    % fast checks to exclude cases where polyZonotope is not a convHull
    if ~isempty(pZ.Grest) && ~all(sum(abs(pZ.Grest),1) == 0)
        return;
    else
        
        if max(pZ.expMat(end,:)) > 1
            return;
        else
            ind0 = find(pZ.expMat(end,:) == 0);
            ind1 = find(pZ.expMat(end,:) == 1);

            if length(ind1) ~= length(ind0) + 1
                return;
            end
        end
    end
    
    % divide into part where last row is 0 and where last row is 1
    expMat0 = pZ.expMat(:,ind0);
    G0 = pZ.G(:,ind0);
    
    expMat1 = pZ.expMat(:,ind1);
    G1 = pZ.G(:,ind1);
    
    % remove constant part
    ind = find(sum(expMat1(1:end-1,:),1) == 0);
    
    if isempty(ind) || length(ind) > 1
        return;
    end
    
    d = G1(:,ind);
    G1(:,ind) = [];
    expMat1(:,ind) = [];
    
    % sort the exponent matrices
    [temp,ind0] = sortrows(expMat0');
    expMat0 = temp';
    
    [temp,ind1] = sortrows(expMat1');
    expMat1 = temp';
    
    if ~all(all(expMat0(1:end-1,:) == expMat1(1:end-1,:)))
        return;
    end
    
    G0 = G0(:,ind0);
    G1 = G1(:,ind1);
    
    found = 0;
    
    % split exponent matrices into the ones for the two polyZonotopes
    for i = 1:size(expMat0,1)-2
        for j = 1:size(expMat0,2)-1
            if expMat0(i,j) == 0 && expMat0(i+1,j+1) == 0
                temp1 = expMat0(1:i,1:j);
                temp2 = expMat0(i+1:end-1,j+1:end);
                if all(all(temp1 == 0)) && all(all(temp2 == 0))
                    
                    % reconstruc the exponent matrices
                    expMat1 = expMat0(i+1:end-1,1:j);
                    expMat2 = expMat0(1:i,j+1:end);
                    
                    % reconstruct the generator vector
                    G1_1 = G0(:,1:j);
                    G2_1 = G0(:,j+1:end);
                    G1_2 = G1(:,1:j);
                    G2_2 = G1(:,j+1:end);
                    
                    % reconstruct center vectors for the polynomial zonotopes
                    if all(all(G1_1 == G1_2)) && all(all(G1_1 == -G1_2))
                        c1 = pZ.c + d;
                        c2 = 2*pZ.c - c1;
                        G1 = 2*G1_1;
                        G2 = 2*G2_1;
                        
                        found = 1;
                        break;
                    elseif all(all(G2_1 == G2_2)) && all(all(G1_1 == -G1_2))
                        c1 = pZ.c - d;
                        c2 = 2*pZ.c - c1;
                        G2 = 2*G2_1;
                        G1 = 2*G1_1;
                        
                        found = 1;
                        break;
                    else
                        continue;
                    end
                end
            end
        end
        if found
           break; 
        end
    end
    
    % construct the resulting polynomial zonotope objects
    pZ1 = polyZonotope(c1,G1,[],expMat1);
    pZ2 = polyZonotope(c2,G2,[],expMat2);
    
    res = 1;
end

function pgon = convHull2D(list1,list2)
% compute the convex hull of two polygons

    pgon = list1{1};
    
    for i = 1:length(list1)
        for j = 1:length(list2)
            temp = convhull(union(list1{i},list2{j}));
            pgon = union(pgon,temp);
        end
    end
end

%------------- END OF CODE --------------