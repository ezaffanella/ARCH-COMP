function [Tf,ind] = thirdOrderTensor_tank2Eq(x,u)



 Tf{1,1} = interval(sparse(3,3),sparse(3,3));

Tf{1,1}(1,1) = -897680497035489/(36028797018963968*x(1)^(5/2));


 Tf{1,2} = interval(sparse(3,3),sparse(3,3));



 Tf{1,3} = interval(sparse(3,3),sparse(3,3));



 Tf{2,1} = interval(sparse(3,3),sparse(3,3));

Tf{2,1}(1,1) = 897680497035489/(36028797018963968*x(1)^(5/2));


 Tf{2,2} = interval(sparse(3,3),sparse(3,3));

Tf{2,2}(2,2) = -897680497035489/(36028797018963968*x(2)^(5/2));


 Tf{2,3} = interval(sparse(3,3),sparse(3,3));


 ind = cell(2,1);
 ind{1} = [1];


 ind{2} = [1;2];

