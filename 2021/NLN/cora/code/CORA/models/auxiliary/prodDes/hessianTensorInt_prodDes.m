function Hf=hessianTensorInt_prodDes(x,u)



 Hf{1} = interval(sparse(4,4),sparse(4,4));

Hf{1}(1,1) = (2*x(2))/(x(1) + 1)^2 - (2*x(1)*x(2))/(x(1) + 1)^3;
Hf{1}(2,1) = x(1)/(x(1) + 1)^2 - 1/(x(1) + 1);
Hf{1}(1,2) = x(1)/(x(1) + 1)^2 - 1/(x(1) + 1);


 Hf{2} = interval(sparse(4,4),sparse(4,4));

Hf{2}(1,1) = (2*x(1)*x(2))/(x(1) + 1)^3 - (2*x(2))/(x(1) + 1)^2;
Hf{2}(2,1) = 1/(x(1) + 1) - x(1)/(x(1) + 1)^2;
Hf{2}(1,2) = 1/(x(1) + 1) - x(1)/(x(1) + 1)^2;


 Hf{3} = interval(sparse(4,4),sparse(4,4));

