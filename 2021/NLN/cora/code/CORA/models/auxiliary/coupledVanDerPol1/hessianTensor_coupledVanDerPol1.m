function Hf=hessianTensor_coupledVanDerPol1(x,u)



 Hf{1} = sparse(5,5);



 Hf{2} = sparse(5,5);

Hf{2}(1,1) = -2*x(2);
Hf{2}(2,1) = -2*x(1);
Hf{2}(1,2) = -2*x(1);


 Hf{3} = sparse(5,5);



 Hf{4} = sparse(5,5);

Hf{4}(3,3) = -2*x(4);
Hf{4}(4,3) = -2*x(3);
Hf{4}(3,4) = -2*x(3);
