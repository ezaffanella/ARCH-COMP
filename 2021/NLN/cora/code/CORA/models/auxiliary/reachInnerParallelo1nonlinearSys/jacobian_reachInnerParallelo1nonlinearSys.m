function [A,B]=jacobian_reachInnerParallelo1nonlinearSys(x,u)

A=[3*x(1)*x(2) - 2,(3*x(1)^2)/2;...
1 - 3*x(1)*x(2),-(3*x(1)^2)/2];

B=[0;...
0];

