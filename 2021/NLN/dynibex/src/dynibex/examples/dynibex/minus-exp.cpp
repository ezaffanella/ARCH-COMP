/* ============================================================================
 * D Y N I B E X - Example for a first validated simulation : system 61 of vericomp
 * ============================================================================
 * Copyright   : ENSTA ParisTech
 * License     : This program can be distributed under the terms of the GNU LGPL.
 *               See the file COPYING.LESSER.
 *
 * Author(s)   : Julien Alexandre dit Sandretto and Alexandre Chapoutot
 * Created     : Jul 18, 2014
 * Sponsored   : This research benefited from the support of the "Chair Complex Systems Engineering - Ecole Polytechnique,
 * THALES, DGA, FX, DASSAULT AVIATION, DCNS Research, ENSTA ParisTech, Telecom ParisTech, Fondation ParisTech and FDO ENSTA"
 * ---------------------------------------------------------------------------- */

#include "ibex.h"

using namespace ibex;

int main(){

  const int n= 1;
  Variable y(n);
  IntervalVector yinit(n);
  yinit[0] = Interval(10.0);

  Function ydot = Function(y, -1.0 * y[0]);

  ivp_ode problem = ivp_ode(ydot,0.0,yinit);

  simulation simu = simulation(&problem, 30.0, RADAU5, 1e-16);

  simu.run_simulation();

  //For an export in order to plot
  simu.export3d_yn("export_3d.txt",0,1,2);

  return 0;
}
