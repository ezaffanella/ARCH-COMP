#include "ibex.h"
#include "vibes.cpp"
#include "vibes.h"
#include <chrono>
using namespace std::chrono;

using namespace ibex;

void plot_simu(const simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
  
  vibes::drawBox(iterator_list->box_j1->operator[](0).lb(), iterator_list->box_j1->operator[](0).ub(),		   iterator_list->box_j1->operator[](1).lb(), iterator_list->box_j1->operator[](1).ub(),"blue[blue]");

  }
}

void plot_simu_in(const simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
  
  vibes::drawBox(iterator_list->box_j1->operator[](0).lb(), iterator_list->box_j1->operator[](0).ub(),		   iterator_list->box_j1->operator[](1).lb(), iterator_list->box_j1->operator[](1).ub(),"green[green]");

  }
}
Interval find_event(simulation* simu, NumConstraint* csp)
{
	std::list<solution_g>::iterator iterator_list;
  
  	CtcFwdBwd fb(*csp);
  	CtcFixPoint ctc(fb,1e-4);
  	double cnt=0.0;
  	Interval event(-1.0);
  	
  		for(iterator_list=simu->list_solution_g.begin();iterator_list!=simu->list_solution_g.end();iterator_list++)
    {
      IntervalVector temp = *(iterator_list->box_j1);
      ctc.contract(temp);
      if (!temp.is_empty())
      {
      	 cnt+=iterator_list->time_j.diam();
      	 if (event.ub()<0.0)
      	 {
      	 	event=iterator_list->time_j;
      	 }
      	 else
      	 	event|=iterator_list->time_j;
      }
}
  cout<< "cnt : " << cnt<<endl;
return event;
}

int main(){

  const int n= 2;
  Variable y(n);

  IntervalVector yinit(n);
  Interval noise(-0.012,0.012);//(-0.004,0.004);
  yinit[0] = Interval(1.3)+noise;
  yinit[1] = Interval(1.0);
  
  Interval alpha(3.0);
  Interval beta(3.0);
  Interval delta(1.0);
  Interval gamma(1.0);

  Function ydot = Function(y,Return(alpha*y[0] - beta*y[0]*y[1],
				    delta*y[0]*y[1] - gamma*y[1]));

  ivp_ode problem = ivp_ode(ydot,0.0,yinit,SYMBOLIC);

  simulation simu = simulation(&problem,1.5,RK4,1e-14, 0.05);

  //simu.active_monotony();
  
  
  //NumConstraint al1(y,sqr(y[0]-1.0) + sqr(y[1]-1.0) < sqr(Interval(0.15)));

  //Array<NumConstraint> alarm(al1);
  //simu.set_alarm(alarm);
  vibes::beginDrawing ();
  vibes::newFigure("Lotka");
  
auto start = high_resolution_clock::now();
  simu.run_simulation();
  

  
    plot_simu(&simu);
    
    
    
  NumConstraint in(y,sqr(y[0]-1.0) + sqr(y[1]-1.0) < sqr(Interval(0.161)));
    NumConstraint out(y,sqr(y[0]-1.0) + sqr(y[1]-1.0) > sqr(Interval(0.161)));
    
  double cnt=0.0;
  Interval ev_time = find_event(&simu,&in);
  cout << ev_time << endl;

    	CtcFwdBwd fb(in);
  	CtcFixPoint ctc(fb,1e-14);

  double tt=ev_time.lb();
  
  IntervalVector last=simu.get_last();
  
  while (tt < ev_time.ub())
  {
  	double h_ev=0.01;
  	IntervalVector init_ev=simu.get(Interval(tt,tt+h_ev));
  	ctc.contract(init_ev);
  	
  	ivp_ode problem_ev = ivp_ode(ydot,tt,init_ev,SYMBOLIC);

  	simulation simu_ev = simulation(&problem_ev,1.5,RK4,1e-14, 0.1);
  	simu_ev.run_simulation();
  	plot_simu_in(&simu_ev);
  	
  	last|=simu_ev.get_last();
  	
  	tt=tt+h_ev;
  	
  }
  
  
  //second step
  ivp_ode problem2 = ivp_ode(ydot,0.0,last,SYMBOLIC);

  simulation simu2 = simulation(&problem2,3.64-1.5,RK4,1e-14, 0.05);
  simu2.run_simulation();
  plot_simu(&simu2);
  
  ev_time = find_event(&simu2,&in);
  cout << ev_time << endl;
  tt=ev_time.lb();
  
  last=simu.get_last();
  while (tt < ev_time.ub())
  {
  	double h_ev=0.01;
  	IntervalVector init_ev=simu2.get(Interval(tt,tt+h_ev));
  	ctc.contract(init_ev);
  	
  	ivp_ode problem_ev = ivp_ode(ydot,tt,init_ev,SYMBOLIC);

  	simulation simu_ev = simulation(&problem_ev,3.54-1.5,RK4,1e-14, 0.1);
  	simu_ev.run_simulation();
  	plot_simu_in(&simu_ev);
  	
  	last|=simu_ev.get_last();
  	
  	tt=tt+h_ev;
  	
  }
  
  
  //simu.generate_python_plot("exp.py", 0, 1);

  auto stop = high_resolution_clock::now();
auto duration = duration_cast<seconds>(stop - start);
cout << "Time taken by function: " << duration.count() << " seconds" << endl;

  cout << "Final volume : " << last.volume() << endl;


  
  vibes::axisLimits(0.6,1.4,0.6,1.4);
  vibes::setFigureProperty("Lotka","width",800);
  vibes::setFigureProperty("Lotka","height",600);
  vibes::drawEllipse(1.0,1.0,0.161,0.161,0.0);
 
  vibes::saveImage("Lotka.jpg","Lotka");
  vibes::closeFigure("Lotka");
  vibes::endDrawing();
  
  return 0;

}
