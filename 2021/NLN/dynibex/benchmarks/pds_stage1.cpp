#include "ibex.h"
#include "vibes.cpp"


using namespace ibex;


void plot_simu(const simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
   /* vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](0).lb(), iterator_list->box_j1->operator[](0).ub(), "red[red]");

    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](1).lb(), iterator_list->box_j1->operator[](1).ub(), "blue[blue]");
*/
    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](2).lb(), iterator_list->box_j1->operator[](2).ub(), "blue[blue]");
  }
}


int main(){

  const int n= 3;
  Variable y(n);

  IntervalVector yinit(n);
  yinit[0] = Interval(9.5,10.0);
  yinit[1] = Interval(0.01);
  yinit[2] = Interval(0.01);
  Interval a(0.3);

  Function ydot = Function(y,Return(-y[0]*y[1]/(1+y[0]),
				    y[0]*y[1]/(1+y[0]) - a*y[1],
				    a*y[1]));

  NumConstraint csp1(y,y[0]+y[1]+y[2] -10.0 = 0);
  NumConstraint csp2(y,y[0]>=0);
  NumConstraint csp3(y,y[1]>=0);
  NumConstraint csp4(y,y[2]>=0);

  Array<NumConstraint> csp(csp1,csp2,csp3,csp4);

  ivp_ode problem = ivp_ode(ydot,0.0,yinit,csp,SYMBOLIC);

  simulation simu = simulation(&problem,100.0,GL4,1e-8, 0.2);

  //simu.active_monotony();

  simu.run_simulation();
  
  std::cout << "Final volume : " << (simu.get_last()).volume() << std::endl;

  vibes::beginDrawing ();
  vibes::selectFigure("PDS");

  plot_simu(&simu);
  
   //vibes::axisAuto();
  vibes::setFigureProperty("PDS","width",800);
  vibes::setFigureProperty("PDS","height",600);
  vibes::saveImage("PDS_1.jpg","PDS");
    
  vibes::closeFigure("PDS");
  vibes::endDrawing();

  return 0;

}
