Repeatability instructions KeYmaera X 4.9.4:

For repeatability, the benchmarks do not use Wolfram Engine due to an incompatibility of JLink with Ubuntu in Wolfram Engine 12.3 (earlier versions <=12.1 would be compatible, but are no longer available from Wolfram). The repeatability package therefore opts to run solely with Z3; as a consequence, Pegasus invariant generation is unavailable and proof automation, especially in the nonlinear HSTP sub-category, is severely limited.

1. Run `setup.sh`, this will initialize the Docker container and build KeYmaera X.
2. Run `measure_all.sh`, this starts the container and runs the benchmarks; after this step, the results are in the results/ subfolder. 