function han = plot(obj,varargin)
% plot - plots 2-dimensional projection of a strips object
%
% Syntax:  
%    han = plot(obj)
%    han = plot(obj,dims)
%    han = plot(obj,dims,type)
%
% Inputs:
%    obj - strips object
%    dims - (optional) dimensions that should be projected (optional);
%          assume that other entries of the normal vector are zeros
%    type - (optional) plot settings (LineSpec and name-value pairs)
%
% Outputs:
%    han - handle to the graphics object
%
% Example: 
%    S = strips([1 1],0.5,1);
% 
%    xlim([-5,5]); ylim([-5,5]);
%    plot(S,[1,2],'r');
%    xlim([-4,4]); ylim([-4,4]);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: mptPolytope/plot

% Author:       Niklas Kochdumper
% Written:      21-November-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    hold on;

    % parse input arguments
    dims = [1,2];
    type{1} = 'b';

    if nargin >= 2 && ~isempty(varargin{1})
        dims = varargin{1};
    end

    if nargin >= 3 && ~isempty(varargin{2})
        type = varargin(2:end); 
    end

    % get size of current plot
    xLim = get(gca,'Xlim');
    yLim = get(gca,'Ylim');

    % loop over all strips
    for i = 1:size(obj.C,1)
    
        % convert to mptPolytope
        S = strips(obj.C(i,:),obj.d(i),obj.y(i));
        poly = mptPolytope(S);

        temp = [eye(2), zeros(2,dim(obj)-2)];
        C = [poly.P.A; temp; -temp];
        d = [poly.P.b; xLim(2); yLim(2); -xLim(1); -yLim(1)];

        poly = mptPolytope(C,d);

        % plot mptPolytope
        han = plot(poly,dims,type{:});
    end

%------------- END OF CODE --------------