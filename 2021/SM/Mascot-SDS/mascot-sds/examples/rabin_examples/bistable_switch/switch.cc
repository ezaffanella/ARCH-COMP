/*
 * switch.cc
 *
 *  created on: 17.02.2021
 *      author: kaushik
 */

/*
 * switched oscillator example (sampled time) with bounded external noise
 * taken from: https://arxiv.org/pdf/2001.09236.pdf
 *
 */

#include <array>
#include <iostream>
#include <math.h>
#include <float.h>
#include <chrono>

#include "boost/lexical_cast.hpp"
#include "cuddObj.hh"

#include "SymbolicSet.hh"
// #include "SymbolicModelGrowthBound.hh"
#include "SymbolicModelMonotonic.hh"

#include "TicToc.hh"
#include "RungeKutta4.hh"
#include "FixedPointRabin.hh"
#include "Helper.hh"

/* state space dim */
#define sDIM 2
#define iDIM 2

/* data types for the ode solver */
typedef std::array<double,sDIM> state_type;
typedef std::array<double,iDIM> input_type;
typedef std::array<double,2> state_xy_dim_type;

/****************************************************************************/
/* main computation
 *
 *  Command line arguments:
 *  argv[1]: Specification: "1" or "2"
 *  argv[2]: State space discretization for dimension 0: eta[0]
 *  argv[3]: State space discretization for dimension 1: eta[1]
 *  argv[4]: (Optional) Name of the logfile (if one of this name doesn't exist, then create one, and if it exists then append to that existing file)
 *  argv[5]: (Optional) Verbosity: 0-2 (Default 0)
 *  argv[6]: (Optional) Acceleration: "true" or "false" (default "true")
 *  argv[7]: (Optional) Memory bound when acceleration is on (default 25); has no effect when acceleration is off
 *  argv[8]: (Optional) Read from file: "true" or "false" (default "false")
 *  argv[9]: (Optional) Warm-start the under-approximation using the outcome of the over-approximation: "true" or "false"
 */
/****************************************************************************/
int main(int argc, char** argv) {
    /* sanity check */
    if (argc<4) {
        std::ostringstream os;
        os << "Error: switch: too few arguments to run switch from command line.";
        throw std::invalid_argument(os.str().c_str());
    }
    int specification;
    specification = boost::lexical_cast<int>(argv[1]);
    double eta[sDIM];
    eta[0] = boost::lexical_cast<double>(argv[2]);
    eta[1] = boost::lexical_cast<double>(argv[3]);
    const char* logfile; /* name of the log file */
    int verbose=0; /* verbose = 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point */
    bool accl_on=true; /* whether accelerated fixpoint is on or not */
    size_t M=25; /* the memory bound (in terms of the maximum number of past iterations to be stored) in case acceleration is on */
    bool RFF=false; /* read transitions from file flag */
    bool warm_start_under_approx=false; /* warm-start the under-approximation from the outcome of the over-approximation */
    if (argc>=5) {
//        logfile=boost::lexical_cast<char*>(argv[4]);
//        argv[4] >> logfile;
        logfile=argv[4];
        std::freopen(logfile, "a", stderr);
    } else {
        /* create a new log file */
        logfile="switch.log";
        std::freopen(logfile, "a", stderr);
    }
    std::clog << "\n\n ******************\n";
    std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::clog << "Time: " << std::ctime(&time) << '\n';
    std::clog << "Specification: " << specification << '\n';
    std::clog << "eta = [" << eta[0] << ", " << eta[1] << "]" << '\n';
    if (argc>=6) {
        verbose=boost::lexical_cast<int>(argv[5]);
    }
    if (argc>=7) {
        accl_on=boost::lexical_cast<bool>(argv[6]);
    }
    if (argc>=8) {
        M=boost::lexical_cast<int>(argv[7]);
    }
    if (argc>=9) {
        RFF=boost::lexical_cast<bool>(argv[8]);
    }
    if (argc>=10) {
        warm_start_under_approx=boost::lexical_cast<bool>(argv[9]);
    }
    
    std::clog << "Verbosity: " << verbose << '\n';
    if (accl_on) {
        std::clog << "Acceleration: ON" << '\n';
    } else {
        std::clog << "Acceleration: OFF" << '\n';
    }
    std::clog << "M: " << M << '\n';
    if (RFF) {
        std::clog << "Read from file: TRUE" << '\n';
    } else {
        std::clog << "Read from file: FALSE" << '\n';
    }
    if (warm_start_under_approx) {
        std::clog << "Warm-start under-approximation: TRUE" << '\n';
    } else {
        std::clog << "Warm-start under-approximation: FALSE" << '\n';
    }
    
    /* ******* the system parameters ********* */
    /* sampling time */
    const double tau = 0.05;
    /* constants */
    const double a = 1.3;
    const double b = 0.25;
    /* upper bounds on the noise which is considered to be centered around the origin */
    const state_type W_ub = {0.1,0.1};
    const double eps = std::min(eta[0],eta[1])/1000; /* a small constant smaller than the state space discretization */

    /* lower bounds of the hyper rectangle */
    double lb[sDIM]={0.0,-1.0}; /* there is a bug that prevents lower bounds at all dimensions be zero */
    double lb_actual[sDIM]={0.0,0.0}; /* real lower bound used to saturate the trajectories */
    /* upper bounds of the hyper rectangle */
    double ub[sDIM]={4.0,4.0};

    /* input space parameters */
    double ilb[iDIM]={-0.05,-0.05};
    double iub[iDIM]={0.05,0.05};
    double ieta[iDIM]={0.05,0.05};
    
    /* the system dynamics (given as a decomposition function of a mixed-monotone system) */
    auto decomposition = [&](input_type &u, state_type &z1, state_type &z2) -> state_type {
        state_type y;
        
        if ((1-a*tau)>=0) {
            y[0] = z1[0] + (-a*z1[0]+z1[1])*tau + u[0] -0.3;
        } else {
            y[0] = z2[0] + (-a*z2[0]+z1[1])*tau + u[0] -0.3;
        }
        if ((1-b*tau)>=0) {
            y[1] = z1[1] + ((pow(z1[0],2)/(pow(z1[0],2)+1)) - b*z1[1])*tau + u[1] -0.3;
        } else {
            y[1] = z2[1] + ((pow(z1[0],2)/(pow(z1[0],2)+1)) - b*z2[1])*tau + u[1] -0.3;
        }
        /* trajectories are maintained in the boundary of the domain */
        y[0] = std::min(y[0],ub[0]-W_ub[0]-eps);
        y[0] = std::max(y[0],lb_actual[0]+W_ub[0]+eps);
        y[1] = std::min(y[1],ub[1]-W_ub[1]-eps);
        y[1] = std::max(y[1],lb_actual[1]+W_ub[1]+eps);
        
        return y;
    };

    /* to measure time */
    TicToc timer;
    /* there is one unique manager to organize the bdd variables */
    Cudd mgr;
    helper::checkMakeDir("data");
    
    /****************************************************************************/
    /* construct SymbolicSet for the state space */
    /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /* the varaibles lb, ub and eta are loaded from input.hh */
    scots::SymbolicSet ss(mgr,sDIM,lb,ub,eta,1);
    ss.addGridPoints();
    /* this part is hacky: we remove the extra part that was added to avoid having the 0 lower bound in both dimensions */
    double K[2*sDIM]={
        0,-1,
        0, 1
    };
    double k[2*sDIM] = {-lb[1],lb_actual[1]-eps};
    ss.remPolytope(2,K,k,scots::OUTER);
    ss.writeToFile("data/X.bdd");
    /****************************************************************************/
    /* construct SymbolicSet for the input space */
    /****************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from input.hh */
    scots::SymbolicSet is(mgr,iDIM,ilb,iub,ieta,0);
    is.addGridPoints();
    /* debug */
    is.writeToFile("data/U.bdd");
    
    /****************************************************************************/
    /* setup class for symbolic model computation */
    /****************************************************************************/
    scots::SymbolicSet sspost(ss,1); /* create state space for post variables */
    scots::SymbolicModelMonotonic<state_type,input_type> abstraction(&ss, &is, &sspost);
    if (RFF) {
        std::cout << "Reading transition relations from file." << '\n';
        scots::SymbolicSet mt(mgr,"data/maybeTransitions.bdd");
        scots::SymbolicSet st(mgr,"data/sureTransitions.bdd");
        abstraction.setTransitionRelations(mt.getSymbolicSet(),st.getSymbolicSet());
        /* get the number of elements in the transition relation */
        std::cout << std::endl << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
    } else {
        /* compute the transition relation */
        timer.tic();
        abstraction.computeTransitions(decomposition, W_ub);
        std::cout << std::endl;
        double time_abstraction = timer.toc();
        std::clog << "Time taken by the abstraction computation: " << time_abstraction << "s.\n";
        /* get the number of elements in the transition relation */
        std::cout << std::endl << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
        /* save transition relations */
        (abstraction.getMaybeTransitions()).writeToFile("data/maybeTransitions.bdd");
        (abstraction.getSureTransitions()).writeToFile("data/sureTransitions.bdd");
    }
    
    /* *************** specification ****************** */
    size_t rabin_nof_states;
    std::vector<BDD> rabin_inputs;
    std::vector<std::array<size_t,3>> rabin_transitions;
    std::vector<std::array<std::vector<size_t>,2>> rabin_pairs;
    if (specification==1) {
        /* state predicates */
        /* add inner approximation of P={ x | H x<= h } form state space */
        double H[4*sDIM]={-1, 0,
            1, 0,
            0,-1,
            0, 1
        };

        double a1[4] = {-1.0+eps,2.0+eps,-1.0+eps,2.0+eps};
        double a2[4] = {-2.0+eps,3.0+eps,-1.0+eps,2.0+eps};
        double a3[4] = {-2.0+eps,3.0+eps,-2.0+eps,3.0+eps};
        
        rabin_nof_states=5;
        /* the inputs are the state predicates: A, B, C, none
         * we assume that A, B, C are non-overlapping */
        scots::SymbolicSet A(ss), none(ss);
        A.addPolytope(4,H,a1,scots::INNER);
        A.addPolytope(4,H,a2,scots::INNER);
        A.addPolytope(4,H,a3,scots::INNER);
        none.addGridPoints();
        none.setSymbolicSet(none.getSymbolicSet() & (!A.getSymbolicSet()));
        rabin_inputs = {
            A.getSymbolicSet(),
            none.getSymbolicSet()
        };
        /* save rabin inputs */
        helper::checkMakeDir("data/rabin_inputs");
        A.writeToFile("data/rabin_inputs/A.bdd");
        none.writeToFile("data/rabin_inputs/none.bdd");
        /* the transitions */
        rabin_transitions ={
            {0,0,0},
            {0,1,1},
    //
            {1,0,2},
            {1,1,1},
    //
            {2,0,3},
            {2,1,4},
    //
            {3,0,0},
            {3,1,4},
    //
            {4,0,4},
            {4,1,4},
        };
        std::vector<size_t> v1 = {0,1};
        std::vector<size_t> v2 = {};
        std::array<std::vector<size_t>,2> pair = {v1,v2};
        rabin_pairs.push_back(pair);
    } else if (specification==2) {
        /* state predicates */
        /* add inner approximation of P={ x | H x<= h } form state space */
        double H[4*sDIM]={-1, 0,
            1, 0,
            0,-1,
            0, 1
        };

        double a1[4] = {0.0+eps,1.0+eps,0.0+eps,1.0+eps};

        double b1[4] = {0.0+eps,1.0+eps,-3.0+eps,4.0+eps};
        double b2[4] = {-1.0+eps,2.0+eps,-1.0+eps,2.0+eps};
        double b3[4] = {-3.0+eps,4.0+eps,-1.0+eps,3.0+eps};

        double c1[4] = {-2.0+eps,3.0+eps,-3.0+eps,4.0+eps};
        
        rabin_nof_states=7;
        /* the inputs are the state predicates: A, B, C, none
         * we assume that A, B, C are non-overlapping */
        scots::SymbolicSet A(ss), B(ss), C(ss), none(ss);
        A.addPolytope(4,H,a1,scots::INNER);
        B.addPolytope(4,H,b1,scots::INNER);
        B.addPolytope(4,H,b2,scots::INNER);
        B.addPolytope(4,H,b3,scots::INNER);
        C.addPolytope(4,H,c1,scots::INNER);
        none.addGridPoints();
        none.setSymbolicSet(none.getSymbolicSet() & (!A.getSymbolicSet()) & (!B.getSymbolicSet()) & (!C.getSymbolicSet()));
        rabin_inputs = {
            A.getSymbolicSet(),
            B.getSymbolicSet(),
            C.getSymbolicSet(),
            none.getSymbolicSet()
        };
        /* save rabin inputs */
        helper::checkMakeDir("data/rabin_inputs");
        A.writeToFile("data/rabin_inputs/A.bdd");
        B.writeToFile("data/rabin_inputs/B.bdd");
        C.writeToFile("data/rabin_inputs/C.bdd");
        none.writeToFile("data/rabin_inputs/none.bdd");
        /* the transitions */
        rabin_transitions ={
            {0,0,1},
            {0,1,5},
            {0,2,2},
            {0,3,0},
    //
            {1,0,1},
            {1,1,5},
            {1,2,2},
            {1,3,0},
    //
            {2,0,3},
            {2,1,4},
            {2,2,2},
            {2,3,2},
    //
            {3,0,3},
            {3,1,4},
            {3,2,2},
            {3,3,2},
    //
            {4,0,4},
            {4,1,4},
            {4,2,4},
            {4,3,4},
    //
            {5,0,5},
            {5,1,5},
            {5,2,6},
            {5,3,5},
    //
            {6,0,6},
            {6,1,6},
            {6,2,6},
            {6,3,6}
        };
        std::vector<size_t> v1 = {0};
        std::vector<size_t> v2 = {1};
        std::array<std::vector<size_t>,2> pair = {v1,v2};
        rabin_pairs.push_back(pair);
        v1={2};
        v2={3};
        pair={v1,v2};
        rabin_pairs.push_back(pair);
        v1={5};
        v2={};
        pair={v1,v2};
        rabin_pairs.push_back(pair);
    } else {
        std::ostringstream os;
        os << "Error: switch: specification is invalid.";
        throw std::invalid_argument(os.str().c_str());
    }
    mascot::RabinAutomaton rabin(mgr, rabin_nof_states, ss, rabin_inputs, rabin_transitions, rabin_pairs);
    /* we setup a fixed point object to compute reach and stay controller */
    scots::FixedPointRabin fp(&abstraction,&rabin);
    std::cout << "FixedPoint initialized." << '\n';
    
    /****************************************************************************/
    /* Over-approximation of almost sure winning region */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of over-approximation of the a.s. controller.\n";
    timer.tic();
    std::vector<BDD> CO = fp.Rabin("over",accl_on,M,&rabin,mgr.bddOne(),verbose);
    /* there will be as many controllers as the number of states of the rabin automaton */
    scots::SymbolicSet controller_over(ss,is);
    helper::checkMakeDir("data/controllers_over");
    for (size_t i=1; i<=CO.size(); i++) {
        scots::SymbolicSet controller_over(ss,is);
        controller_over.setSymbolicSet(CO[i-1]);
        std::string Str = "";
        Str += "data/controllers_over/C";
        Str += std::to_string(i);
        Str += ".bdd";
        char Char[20];
        size_t Length = Str.copy(Char, Str.length() + 1);
        Char[Length] = '\0';
        controller_over.writeToFile(Char);
    }
    double time_over_approx = timer.toc();
    std::clog << "Time taken by the over-approximation computation: " << time_over_approx << "s.\n";
    std::clog << "Number of states in the over-approximation: " << CO[0].ExistAbstract(fp.getCubeInput() * rabin.stateSpace_->getCube()).CountMinterm(ss.getNVars()) << std::endl;
    
    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    BDD initial_seed;
    if (warm_start_under_approx) {
        std::cout << "\n\n Starting computation of under-approximation of the a.s. controller (w/ initial seed).\n";
        initial_seed=mgr.bddZero();
        for (size_t i=0; i<CO.size(); i++) {
            initial_seed |= CO[i];
        }
    } else {
        std::cout << "\n\n Starting computation of under-approximation of the a.s. controller (w/o initial seed).\n";
        initial_seed=mgr.bddOne();
    }
    
    timer.tic();
    std::vector<BDD> CU = fp.Rabin("under",accl_on,M,&rabin,initial_seed,verbose);
    /* there will be as many controllers as the number of states of the rabin automaton */
    scots::SymbolicSet controller_under(ss,is);
    helper::checkMakeDir("data/controllers");
    for (size_t i=1; i<=CU.size(); i++) {
        scots::SymbolicSet controller_under(ss,is);
        controller_under.setSymbolicSet(CU[i-1]);
        std::string Str = "";
        Str += "data/controllers/C";
        Str += std::to_string(i);
        Str += ".bdd";
        char Char[20];
        size_t Length = Str.copy(Char, Str.length() + 1);
        Char[Length] = '\0';
        controller_under.writeToFile(Char);
    }
    double time_under_approx = timer.toc();
    std::clog << "Time taken by the under-approximation computation w/ initial seed: " << time_under_approx << "s.\n";
    std::clog << "Total time for synthesis: " << time_over_approx+time_under_approx << "s.\n";
    std::clog << "Number of states in the under-approximation: " << CU[0].ExistAbstract(fp.getCubeInput() * rabin.stateSpace_->getCube()).CountMinterm(ss.getNVars()) << std::endl;
    std::clog << "Absolute volume of the error region: " << (CO[0].ExistAbstract(fp.getCubeInput() * rabin.stateSpace_->getCube()).CountMinterm(ss.getNVars()) - CU[0].ExistAbstract(fp.getCubeInput() * rabin.stateSpace_->getCube()).CountMinterm(ss.getNVars()))*eta[0]*eta[1] << std::endl;

    
    return 1;
}
