So far, reachable sets of linear continuous systems have been presented. Although a fairly large group of dynamic systems can be described by linear continuous systems, the extension to nonlinear continuous systems is an important step for the analysis of more complex systems. The analysis of nonlinear systems is much more complicated since many valuable properties are no longer valid. One of them is the superposition principle, which allows the homogeneous and the inhomogeneous solution to be obtained separately. Another is that reachable sets of linear systems can be computed by a linear map. This makes it possible to exploit that geometric representations such as ellipsoids, zonotopes, and polytopes are closed under linear transformations, i.e., they are again mapped to ellipsoids, zonotopes and polytopes, respectively. In CORA, reachability analysis of nonlinear systems is based on abstraction. We present abstraction by linear systems as presented in \cite[Section 3.4]{Althoff2010a} and by polynomial systems as presented in \cite{Althoff2013a}. Since the abstraction causes additional errors, the abstraction errors are determined in an over-approximative way and added as an additional uncertain input so that an over-approximative computation is ensured. 

General nonlinear continuous systems with uncertain parameters and Lipschitz continuity are considered. In analogy to the previous linear systems, the initial state $x(0)$ can take values from a set $\mathcal{X}_0 \subset \mathbb{R}^{n}$ and the input $u$ takes values from a set $\mathcal{U} \subset \mathbb{R}^{m}$. The evolution of the state $x$ is defined by the following differential equation:
\begin{gather}\label{eq_nonlinSystem}
	\dot{x}(t) = f(x(t),u(t)), \qquad
	x(0)\in \mathcal{X}_0 \subset \mathbb{R}^{n}, \quad u(t)\in \mathcal{U} \subset \mathbb{R}^{m}, \notag
\end{gather} 
where $u(t)$ and $f(x(t),u(t))$ are assumed to be globally Lipschitz continuous so that the Taylor expansion for the state and the input can always be computed, a condition required for the abstraction. 


\begin{figure}[ht!] 
\psfrag{1}[c][c]{\ding{192}}
\psfrag{2}[c][c]{\ding{193}}
\psfrag{3}[c][c]{\ding{194}}
\psfrag{4}[c][c]{\ding{195}}
\psfrag{5}[c][c]{\ding{196}}
\psfrag{6}[c][c]{\ding{197}}
\psfrag{7}[c][c]{\ding{198}}


\psfrag{a}[c][c]{Initial set: $\mathcal{R}(0)=\mathcal{X}_0$, time step: $k=1$}	
\psfrag{b}[c][c]{Compute system abstraction (linear/polynomial)}									 
\psfrag{c}[c][c]{Obtain required abstraction errors $\bar{\mathcal{L}}$ heuristically}			
\psfrag{d}[c][c]{Compute $\mathcal{R}^{abstract}(\tau_k)$ of $\dot{x}(t) \in f^{abstract}(x(t),u(t)) \oplus \bar{\mathcal{L}}$} 
\psfrag{e}[c][c]{Compute $\mathcal{L}$ based on $\mathcal{R}^{abstract}(\tau_k)$}	
\psfrag{q}[c][c]{$\mathcal{L} \subseteq \bar{\mathcal{L}}$ ?}
\psfrag{f}[c][c]{Enlarge $\bar{\mathcal{L}}$}	
\psfrag{g}[c][c]{Compute $\mathcal{R}(\tau_k)$ of $\dot{x}(t) \in f^{abstract}(x(t),u(t)) \oplus \mathcal{L}$}		
\psfrag{h}[c][c]{Cancellation of redundant reachable sets}	
\psfrag{i}[c][c]{Next initial set: $\mathcal{R}(t_{k+1})$, time step: $k:=k+1$}			
\psfrag{y}[c][c]{Yes}	
\psfrag{n}[c][c]{No}	

	\centering
      \includegraphics[width=0.75\columnwidth]{./figures/nonlinearOverview.eps}
      \caption{Computation of reachable sets -- overview.}
      \label{fig:nonLinOverview}
\end{figure}

A brief visualization of the overall concept for computing the reachable set is shown in Fig.~\ref{fig:nonLinOverview}. As in the previous approaches, the reachable set is computed iteratively for time intervals $t\in \tau_k = [k \, r, (k+1)r]$ where $k \in \mathbb{N}^+$. The procedure for computing the reachable sets of the consecutive time intervals is as follows: 

\begin{itemize}

\item[\ding{192}] The nonlinear system $\dot{x}(t)= f(x(t),u(t))$ is either abstracted to a linear system as shown in \eqref{eq:linearSystemSimple}, or after introducing $z= [x^T, \, u^T]^T$, to a polynomial system of the form
\begin{equation} \label{eq:polynomialEquation}
\begin{split}
 \dot{x}_i =f^{abstract}(x,u) = & w_i + \frac{1}{1!} \sum_{j=1}^o C_{ij}z_j(t) + \frac{1}{2!} \sum_{j=1}^o \sum_{k=1}^o D_{ijk} z_j(t) z_k(t) \\
            & + \frac{1}{3!} \sum_{j=1}^o \sum_{k=1}^o \sum_{l=1}^o E_{ijkl} z_j(t) z_k(t) z_l(t) + \ldots  
\end{split}
\end{equation}
The set of abstraction errors $\mathcal{L}$ ensures that $f(x,u)\in f^{abstract}(x,u) \oplus \mathcal{L}$, which allows the reachable set to be computed in an over-approximative way. 

\item[\ding{193}] Next, the set of required abstraction errors $\bar{\mathcal{L}}$ is obtained heuristically.

\item[\ding{194}] The reachable set $\mathcal{R}^{abstract}(\tau_k)$ of $\dot{x}(t) \in f^{abstract}(x(t),u(t)) \oplus \bar{\mathcal{L}}$ is computed.

\item[\ding{195}] The set of abstraction errors $\mathcal{L}$ is computed based on the reachable set $\mathcal{R}^{abstract}(\tau_k)$. 

\item[\ding{196}] When $\mathcal{L} \nsubseteq \bar{\mathcal{L}}$, the abstraction error is not admissible, requiring the assumption $\bar{\mathcal{L}}$ to be enlarged. If several enlargements are not successful, one has to split the reachable set and continue with one more partial reachable set from then on. 

\item[\ding{197}] If $\mathcal{L} \subseteq \bar{\mathcal{L}}$, the abstraction error is accepted and the reachable set is obtained by using the tighter abstraction error: $\dot{x}(t) \in f^{abstract}(x(t),u(t)) \oplus \mathcal{L}$. 

\item[\ding{198}] It remains to increase the time step ($k:=k+1$) and cancel redundant reachable sets that are already covered by previously computed reachable sets. This decreases the number of reachable sets that have to be considered in the next time interval. 
\end{itemize}

The necessity of splitting reachable sets is indicated in the workspace outputs using the keyword \texttt{split}. The ratio of the current abstraction errors to the allowed abstraction errors is indicated in the workspace outputs using the keyword \texttt{perfInd}. If $\mathtt{perfInd >=1}$, the reachable set has to be split. 

\subsubsection{Method \texttt{initReach}} \label{sec:nonlinearSystem_initReach}

The method \texttt{initReach} computes the reachable set for a first point in time $r$ and the first time interval $[0,r]$. In contrast to linear systems, it is required to call \texttt{initReach} for each time interval $\tau_k$ since the system is abstracted for each time interval $\tau_k$ by a different abstraction $f^{abstract}(x,u)$. 

The following functions take care of the most relevant computations:
\begin{itemize}
 \item \texttt{linReach} -- computes the reachable set of the abstraction $f^{abstract}(x(t),u(t)) \oplus \bar{\mathcal{L}}$ and returns if
the initial set has to be split in order to control the abstraction error. The name of the function has historical reasons and will change. 
 \item \texttt{linearize} -- linearizes the nonlinear system.
 \item \texttt{linError\_mixed\_noInt} -- computes the linearization error without use of
interval arithmetic according to \cite[Theorem 1]{Althoff2014a}. 
 \item \texttt{linError\_thirdOrder} -- computes abstraction errors according to \cite[Section 4.1]{Althoff2013a}.
 \item \texttt{linError} -- easiest, but also most conservative computation of the linearization error according to \cite[Proposition 1]{Althoff2008c}. 
\end{itemize}