function pass = test_spaceex2cora_nonlinear_vanDerPol()
% test_spaceex2cora_nonlinear_vanDerPol - example of nonlinear reachability
%
% Syntax:  
%    test_spaceex2cora_nonlinear_vanDerPol
%
% Inputs:
%    no
%
% Outputs:
%    pass - boolean 
% 
% Author:       Matthias Althoff, Raja Judeh
% Written:      26-June-2009
% Last update:  13-September-2018
% Last revision:15-January-2019 (SX_HA to SX_nonLin)

%------------- BEGIN CODE --------------

%% Set options

options.tStart = 0; %start time
options.tFinal = 6.74; %final time
options.x0 = [1.4; 2.3; 0]; %initial state for simulation I ADDED THE FINAL VALUE

Z0{1} = zonotope([1.4 0.3 0; 2.3 0 0.05]); %initial state for reachability analysis

options.R0 = zonotopeBundle(Z0);
options.timeStep = 0.0001; %time step size for reachable set computation
options.taylorTerms = 4; %number of taylor terms for reachable sets
options.zonotopeOrder = 10; %zonotope order
options.polytopeOrder = 1.5; %polytope order

options.advancedLinErrorComp = 0;
options.tensorOrder = 1;
options.reductionTechnique = 'girard';
options.filterLength = [10, 5];
options.maxError = 0.05*[1; 1];
options.reductionInterval = 100;
options.verbose = 1;

% Uncertain inputs
options.uTrans = 0;
options.U = zonotope(0); 
options.u = 0;

%% Specify continuous dynamics

vanDerPolSys = nonlinearSys(2,1,@vanderPolEq,options); %original system

%% Convert the spaceEx file into CORA model

spaceex2cora('vanDerPol.xml')
vanDerPolSys_SX = vanDerPol(); %converted system

%% Simulation

% Simulating the original system
[~, simResOriginal.t, simResOriginal.x, ~] = simulate(vanDerPolSys, options, ...
                                                      options.tStart, options.tFinal, ...
                                                      options.x0(1:2), options);
                             
% Simulating the converted system
[~, simResSX.t, simResSX.x, ~] = simulate(vanDerPolSys_SX, options, ...
                                                      options.tStart, options.tFinal, ...
                                                      options.x0(1:2), options);

%% Compute error between the simulation of the two files

diff = simResOriginal.x - simResSX.x;
error = sqrt(sum(diff.^2,2));

if any(error > 1e-5) 
    disp('Failed Conversion: error = ' + string(max(error)));
    pass = false;
    return
end 

disp('Sucessful Conversion: error = ' + string(max(error)))
pass = true;

end

%------------- END OF CODE --------------