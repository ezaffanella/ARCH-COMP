function incMat = icidenceMatrix(~)
% incidenceMatrix - returns the incidence matrix of the markers; 1 if the
% markers are connected and 0 otherwise
% 
%
% Syntax:  
%    incMat = icidenceMatrix(~)
%
% Inputs:
%    -
%
% Outputs:
%    incMat - incidence matrix
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      10-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% markers
% M1: Left front waist (LFWT)
% M2: Right front waist (RFWT)
% M3: Left back waist (LBWT)
% M4: Right back waist (RBWT)
% M5: Left thigh (LTHI)
% M6: Left knee (LKNE)
% M7: Left shin (LSHN)
% M8: Left ankle (LANK)
% M9: Left heel (LHEE)
% M10: Left metatarsal five (LMT5)
% M11: Left toe (LTOE)
% M12: Right Thigh (RTHI)
% M13: Right knee (RKNE)
% M14: Right shin offset (RSHN)
% M15: Right ankle (RANK)
% M16: Right heel (RHEE)
% M17: Right metatarsal five (RMT5)
% M18: Right toe (RTOE)
% M19: Clavicle (CLAV)
% M20: Sternum (STRN)
% M21: Back of neck (C7)
% M22: Upper back (T10)
% M23: Right back offset marker (RBAC)
% M24: Left front head (LFHD)
% M25: Right front head (RFHD)
% M26: Left back head (LBHD)
% M27: Right back head (RBHD)
% M28: Left shoulder (LSHO)
% M29: Left upper arm (LUPA)
% M30: Left elbow (LELB)
% M31: Left forearm (LFRM)
% M32: Left wrist bar thumb side (LWRA)
% M33: Left wrist bar pinkie side (LWRB)
% M34: Left hand (LFIN)
% M35: Right shoulder (RSHO)
% M36: Right upper arm (RUPA)
% M37: Right elbow (RELB)
% M38: Right forearm (RFRM)
% M39: Right wrist bar thumb side (RWRA)
% M40: Right wrist bar pinkie side (RWRB)
% M41: Right hand (RFIN)

% which marker should have a constant distance with which other marker? (incidence matrix)
incMat = sparse(zeros(41)); % init

% neighbors of M1: Left front waist (LFWT)
incMat(1,2) = 1; % RFWT
incMat(1,3) = 1; % LBWT
incMat(1,4) = 1; % RBWT
incMat(1,5) = 1; % LTHI

% neighbors of M2: Right front waist (RFWT)
incMat(2,1) = 1; % LFWT
incMat(2,3) = 1; % LBWT
incMat(2,4) = 1; % RBWT
incMat(2,12) = 1; % RTHI

% neighbors of M3: Left back waist (LBWT)
incMat(3,1) = 1; % LFWT
incMat(3,2) = 1; % RFWT
incMat(3,4) = 1; % RBWT
incMat(3,5) = 1; % LTHI

% neighbors of M4: Right back waist (RBWT)
incMat(4,1) = 1; % LFWT
incMat(4,2) = 1; % RFWT
incMat(4,3) = 1; % LBWT
incMat(4,12) = 1; % RTHI

% neighbors of M5: Left thigh (LTHI)
incMat(5,1) = 1; % LFWT
incMat(5,3) = 1; % LBWT
incMat(5,6) = 1; % LKNE

% neighbors of M6: Left knee (LKNE)
incMat(6,5) = 1; % LTHI
incMat(6,7) = 1; % LSHN
incMat(6,8) = 1; % LANK

% neighbors of M7: Left shin (LSHN)
incMat(7,6) = 1; % LKNE
incMat(7,8) = 1; % LANK

% neighbors of M8: Left ankle (LANK)
incMat(8,6) = 1; % LKNE
incMat(8,7) = 1; % LSHN
incMat(8,9) = 1; % LHEE
incMat(8,10) = 1; % LMT5
incMat(8,11) = 1; % LTOE

% neighbors of M9: Left heel (LHEE)
incMat(9,8) = 1; % LANK
incMat(9,10) = 1; % LMT5
incMat(9,11) = 1; % LTOE

% neighbors of M10: Left metatarsal five (LMT5)
incMat(10,8) = 1; % LANK
incMat(10,9) = 1; % LHEE
incMat(10,11) = 1; % LTOE

% neighbors of M11: Left toe (LTOE)
incMat(11,8) = 1; % LANK
incMat(11,9) = 1; % LHEE
incMat(11,10) = 1; % LMT5

% neighbors of M12: Right Thigh (RTHI)
incMat(12,2) = 1; % RFWT
incMat(12,4) = 1; % RBWT
incMat(12,13) = 1; % RKNE

% neighbors of M13: Right knee (RKNE)
incMat(13,12) = 1; % RTHI
incMat(13,14) = 1; % RSHN
incMat(13,15) = 1; % RANK

% neighbors of M14: Right shin offset (RSHN)
incMat(14,13) = 1; % RKNE
incMat(14,15) = 1; % RANK

% neighbors of M15: Right ankle (RANK)
incMat(15,13) = 1; % RKNE
incMat(15,14) = 1; % RSHN
incMat(15,16) = 1; % RHEE
incMat(15,17) = 1; % RMT5
incMat(15,18) = 1; % RTOE

% neighbors of M16: Right heel (RHEE)
incMat(16,15) = 1; % RANK
incMat(16,17) = 1; % RMT5
incMat(16,18) = 1; % RTOE

% neighbors of M17: Right metatarsal five (RMT5)
incMat(17,15) = 1; % RANK
incMat(17,16) = 1; % RHEE
incMat(17,18) = 1; % RTOE

% neighbors of M18: Right toe (RTOE)
incMat(18,15) = 1; % RANK
incMat(18,16) = 1; % RHEE
incMat(18,17) = 1; % RMT5

% neighbors of M19: Clavicle (CLAV)
incMat(19,20) = 1; % STRN
incMat(19,21) = 1; % C7
incMat(19,28) = 1; % LSHO
incMat(19,35) = 1; % RSHO

% neighbors of M20: Sternum (STRN)
incMat(20,19) = 1; % CLAV
incMat(20,22) = 1; % T10

% neighbors of M21: Back of neck (C7)
incMat(21,19) = 1; % CLAV
incMat(21,22) = 1; % T10
incMat(21,23) = 1; % RBAC
incMat(21,28) = 1; % LSHO
incMat(21,35) = 1; % RSHO

% neighbors of M22: Upper back (T10)
incMat(22,20) = 1; % STRN
incMat(22,21) = 1; % C7
incMat(22,23) = 1; % RBAC

% neighbors of M23: Right back offset marker (RBAC)
incMat(23,21) = 1; % C7
incMat(23,22) = 1; % T10
incMat(23,35) = 1; % RSHO

% neighbors of M24: Left front head (LFHD)
incMat(24,25) = 1; % RFHD
incMat(24,26) = 1; % LBHD
incMat(24,27) = 1; % RBHD

% neighbors of M25: Right front head (RFHD)
incMat(25,24) = 1; % LFHD
incMat(25,26) = 1; % LBHD
incMat(25,27) = 1; % RBHD

% neighbors of M26: Left back head (LBHD)
incMat(26,24) = 1; % LFHD
incMat(26,25) = 1; % RFHD
incMat(26,27) = 1; % RBHD

% neighbors of M27: Right back head (RBHD)
incMat(27,24) = 1; % LFHD
incMat(27,25) = 1; % RFHD
incMat(27,26) = 1; % LBHD

% neighbors of M28: Left shoulder (LSHO)
incMat(28,19) = 1; % CLAV
incMat(28,21) = 1; % C7
incMat(28,29) = 1; % LUPA
incMat(28,30) = 1; % LELB

% neighbors of M29: Left upper arm (LUPA)
incMat(29,28) = 1; % LSHO
incMat(29,30) = 1; % LELB

% neighbors of M30: Left elbow (LELB)
incMat(30,28) = 1; % LSHO
incMat(30,29) = 1; % LUPA
incMat(30,31) = 1; % LFRM
incMat(30,32) = 1; % LWRA
incMat(30,33) = 1; % LWRB

% neighbors of M31: Left forearm (LFRM)
incMat(31,30) = 1; % LELB
incMat(31,32) = 1; % LWRA
incMat(31,33) = 1; % LWRB

% neighbors of M32: Left wrist bar thumb side (LWRA)
incMat(32,30) = 1; % LELB
incMat(32,31) = 1; % LFRM
incMat(32,33) = 1; % LWRB
incMat(32,34) = 1; % LFIN

% neighbors of M33: Left wrist bar pinkie side (LWRB)
incMat(33,30) = 1; % LELB
incMat(33,31) = 1; % LFRM
incMat(33,32) = 1; % LWRA
incMat(33,34) = 1; % LFIN

% neighbors of M34: Left hand (LFIN)
incMat(34,32) = 1; % LWRA
incMat(34,33) = 1; % LWRB

% neighbors of M35: Right shoulder (RSHO)
incMat(35,19) = 1; % CLAV
incMat(35,21) = 1; % C7
incMat(35,23) = 1; % RBAC
incMat(35,36) = 1; % RUPA
incMat(35,37) = 1; % RELB

% neighbors of M36: Right upper arm (RUPA)
incMat(36,35) = 1; % RSHO
incMat(36,37) = 1; % RELB

% neighbors of M37: Right elbow (RELB)
incMat(37,35) = 1; % RSHO
incMat(37,36) = 1; % RUPA
incMat(37,38) = 1; % RFRM
incMat(37,39) = 1; % RWRA
incMat(37,40) = 1; % RWRB

% neighbors of M38: Right forearm (RFRM)
incMat(38,37) = 1; % RELB
incMat(38,39) = 1; % RWRA
incMat(38,40) = 1; % RWRB

% neighbors of M39: Right wrist bar thumb side (RWRA)
incMat(39,37) = 1; % RELB
incMat(39,38) = 1; % RFRM
incMat(39,40) = 1; % RWRB
incMat(39,41) = 1; % RFIN

% neighbors of M40: Right wrist bar pinkie side (RWRB)
incMat(40,37) = 1; % RELB
incMat(40,38) = 1; % RFRM
incMat(40,39) = 1; % RWRA
incMat(40,41) = 1; % RFIN

% neighbors of M41: Right hand (RFIN)
incMat(41,39) = 1; % RWRA
incMat(41,40) = 1; % RWRB

% check symmetry of incidence matrix
symmetric = all(all(incMat == incMat'));

if ~symmetric
    disp('incidence matrix is not symmetric')
end


%------------- END OF CODE --------------