The following steps are required to set up a docker container with MATLAB and CORA running it.


1. Copy your local MATLAB installation

It is required that MATLAB is installed on your machine. Copy all the files of your local matlab installation (e.g., all files in the folder /usr/local/MATLAB/R2019a/) to the 2019/AFF/cora/MATLAB folder in this repository.


2. Execute docker as the user associated with your MATLAB licence

For MATLAB to run successfully inside the docker container, it is important that docker is executed by the same Linux user ($USER) that is associated with your MATLAB licence. The following commands allow you to execute docker as a non-root user:

sudo groupadd docker
sudo usermod -aG docker $USER

After running these commands, restart your computer so that the group memberships are re-evaluated.


3. Create the directory for the results

Go to the directory 2019/AFF/cora/ and create the directory results with the following command:

mkdir results

It is important that the directory is created by the Linux user who is associated with your MATLAB licence.


4. Modify the Dockerfile

In line 3 of the dockerfile (2019/AFF/cora/Dockerfile), change the username from "testuser" to the name of the user that is associated with your MATLAB licence (e.g., ENV user testuser -> ENV user niklas)


5. Build the docker container

First, go to the directory 2019/AFF/cora/. Run the following command to build the docker container with MATLAB and CORA installed in it:

docker build -t docker_cora .


6. Run ARCH benchmarks

To execute the ARCH benchmarks, run the docker container with the following command:

docker run -v $RESULTSPATH:/home/$USER/results --user=$USER --mac-address=$MACADDRESS /bin/bash -c "cd bin; ./matlab -nodesktop -nosplash -r \"cd ..; cd ..; addpath(genpath('CORA')); install_mpt; exec_ARCH19_linear('results'); exit;\""

Exchange $USER by the username of the Linux user that is associated with your MATLAB licence, $MACADDRESS by the mac-address of your computer, and $RESULTSPATH with the full path to the results-folder created in step 3.


